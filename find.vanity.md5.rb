#!/usr/bin/env ruby

class Words
    def initialize
        @next=' '
    end

    def next
        z = @next
        increment
        return z
    end

    def increment
        i = @next.length - 1

        while i >= 0
            c = @next[i].ord
            if c == 126
                @next[i]='!'
            else
                @next[i]=(c+1).chr
                return
            end
            i -= 1
        end
        @next = '!' + @next
    end
end

require 'digest'

require 'date'

md5 = Digest::MD5.new
generator = Words.new

vanity = '314159265358979323'
search = 0
look_for = vanity[0..search]

print_speed = 1000000
start_time = DateTime.now.to_time.to_f

i = 0
j = 0
while true
    word = generator.next
    encoded = md5.hexdigest word
    i+=1
    j+=1
    if j == print_speed
        new_time = DateTime.now.to_time.to_f
        printf "%d hashes in %.3fs\n", print_speed, new_time - start_time
        start_time = new_time;
        j = 0
    end
    next unless encoded.start_with? look_for
    printf "%10d : %-20s : %s %s\n", i, word, look_for, encoded[search..encoded.size]
    search += 1
    break if search == vanity.length
    look_for = vanity[0..search]
end
