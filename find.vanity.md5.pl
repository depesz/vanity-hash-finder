#!/usr/bin/env perl

package Words;

sub new {
    my $class = shift;
    my $self = bless {}, $class;
    $self->{ 'next' } = [ 32 ];
    return $self;
}

sub next {
    my $self = shift;
    $self->increment();
    return pack( 'C*', @{ $self->{ 'next' } } );
}

sub increment {
    my $self = shift;
    my $N    = $self->{ 'next' };
    $N->[ -1 ]++;

    for ( my $i = $#{ $N } ; $i ; $i-- ) {
        return if $N->[ $i ] < 127;
        $N->[ $i ] = 33;
        $N->[ $i - 1 ]++;
    }

    if ( $N->[ 0 ] == 127 ) {
        $N->[ 0 ] = 33;
        unshift @{ $N }, 33;
    }
}

package main;

use Digest::MD5 qw( md5_hex );
use Time::HiRes qw( time );

$SIG{ INT } = sub { exit };

my $generator = Words->new();

my $vanity   = '314159265358979323';
my $search   = 1;
my $look_for = substr( $vanity, 0, $search );

my $print_speed = 1000000;
my $start_time  = time();

my $i = 0;
my $j = 0;
while ( 1 ) {
    my $word   = $generator->next();
    my $digest = md5_hex( $word );
    $i++;
    $j++;
    if ( $j == $print_speed ) {
        my $new_time = time();
        printf "%d hashes in %.3fs\n", $print_speed, $new_time - $start_time;
        $start_time = $new_time;
        $j          = 0;
    }
    next unless $look_for eq substr( $digest, 0, $search );
    printf "%10d : %-20s : %-10s : %s\n", $i, $word, $look_for, $digest;
    $search += 1;
    last if $search == length( $vanity );
    $look_for = substr( $vanity, 0, $search );
}
