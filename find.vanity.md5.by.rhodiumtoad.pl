#!/usr/bin/perl -w
# Based on https://pgsql.privatepaste.com/1c79017e6b
# Modifications by depesz:
# - added default initial candidate
# - added code to show speed every 1000000 hashes

use strict;
use Digest::MD5 qw(md5_hex);
use Time::HiRes qw(time);

my $target = "314159265358979323";
my $candidate = shift @ARGV || 'a';
my $best = "";
my $best_len = 1;
my $continue = 1;
my $start_time = time();
my $print_speed = 1000000;
my $loop = 0;

$SIG{INT} = sub { $continue = 0; };

for (; $continue; ++$candidate)
{
    my $hash = md5_hex($candidate);
    if ( ++$loop == $print_speed ) {
        my $new_time = time();
        printf "%d hashes in %.3fs\n", $print_speed, $new_time - $start_time;
        $start_time = $new_time;
        $loop = 0;
    }
    if (substr($hash, 0, $best_len + 1) eq substr($target, 0, $best_len + 1))
    {
        $best = $candidate;
        ($hash ^ $target) =~ /^(\x00+)/ and $best_len = length($1);
        print "$best $hash ($best_len)\n";
    }
}

print "next try $candidate\n";
exit 0;
